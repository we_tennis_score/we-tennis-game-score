LN = '\n'
class scores:
    def __init__(self):
        self.set = 0
        self.game = 0
        self.point = 0
def points_increment(Player: scores):
            if Player.point < 30:
                Player.point += 15
            else:
                Player.point += 10
            return Player.point
def game_won(Player1: scores, Player2: scores):
        if Player1.point > 40 and Player1.point - Player2.point >= 20:
            Player1.game += 1
            Player1.point = 0
            Player2.point = 0
        if Player2.point > 40 and Player2.point - Player1.point >= 20: 
            Player2.game += 1
            Player1.point = 0
            Player2.point = 0
        return 
def set_won(Player1, Player2):
        if Player1.game == 6:
            Player1.set += 1
        if Player2.game == 6:
            Player2.set += 1
def game_and_points_won(play_won: str) -> str:
    # A.set, B.set, A.game, B.game, A.point, B.point = 0,0,0,0,0,0
    Player1 = scores()
    Player2 = scores()
    for ch in play_won:
        #print(ch)
        if ch == "A":
            Player1.points = points_increment(Player1)
        else:
            Player2.points = points_increment(Player2)
        game_won(Player1, Player2)
        set_won(Player1, Player2)
    return f'S {Player1.set} {Player2.set}{LN}G {Player1.game} {Player2.game}{LN}P {Player1.point} {Player2.point}'
print(game_and_points_won("ABABBBBBABABABBB"))
